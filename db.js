const {Sequelize} = require('sequelize')
const databaseName = process.env.NODE_ENV === 'production' ? process.env.DB_PROD_NAME : process.env.DB_DEV_NAME
const databaseUser = process.env.DB_USER
const databasePassword = process.env.DB_PASSWORD
const host = process.env.NODE_ENV === 'production' ? process.env.DB_PROD_HOST : process.env.DB_DEV_HOST
const port = process.env.DB_PORT

module.exports = new Sequelize(
    databaseName,
    databaseUser,
    databasePassword,
    {
        dialect: 'postgres',
        host,
        port
    }
)
