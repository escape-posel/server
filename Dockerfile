FROM node:20-alpine

WORKDIR /usr/src/server
COPY . /usr/src/server
RUN ls -a
RUN npm install

CMD ["node", "index"]